#!/usr/bin/env python
# license removed for brevity
import rospy
import std_msgs
from sensor_msgs.msg import NavSatFix
import nav_msgs
import socket

class driver(object):
	def __init__(self):		
		s = socket.socket() # create a socket object
		host = rospy.get_param('~host', '172.20.10.4')
		port = rospy.get_param('~port', 3000)
		s.connect((host, port))
		GPS = s.recv(1024)
		

	def talker():
		pub = rospy.Publisher('/rtk_fix', NavSatFix, queue_size=10) # topic named /rtk_fix
		rospy.init_node('rtk_gps_node', anonymous=True)		    # node name rtk_gps_node
		rate = rospy.Rate(1) #1Hz
		while not rospy.is_shutdown():
			hello_str = "hello world %s" % rospy.get_time()
			rospy.loginfo(hello_str)
			pub.publish(hello_str)
			rate.sleep()


if __name__ == '__main__':
	try:
		talker()
	except	rospy.ROSInterruptException:
		pass

 
