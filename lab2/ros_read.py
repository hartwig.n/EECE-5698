#!/usr/bin/env python
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import numpy as np
import rosbag

def readBagTopicList():

	inputFileName = 'stationary-pg.bag'
	#inputFileName = 'stationary-isec.bag'
	#inputFileName = 'moving-pg.bag'
	#inputFileName = 'moving-isec.bag'
	#inputFileName = 'almost-square-isec.bag'
	#inputFileName = 'moving-pg-2.bag'


	bag = rosbag.Bag(inputFileName)

	utm_x = []
	utm_y = []
	z_data = []
	j = 0		

	for topic, msg, t in bag.read_messages(topics=['/utm_fix']):		
				
		utm_x.append(float(msg.pose.pose.position.x))
		utm_y.append(float(msg.pose.pose.position.y))
	bag.close()

	print(utm_x[:50])
	print('\n\n\n')
	print(utm_y[:50])
	
	utm_first_val_y = utm_y[1]	
	utm_first_val_x = utm_x[1]

	for j in range(len(utm_x)):		
		z_data.append(float(j))
		utm_x[j] = utm_x[j] - utm_first_val_x #328064.1478229133
		utm_y[j] = utm_y[j] - utm_first_val_y #4689351.206277389	

	avg_utm_x = np.mean(utm_x)
	avg_utm_y = np.mean(utm_y)
	
	print('avg_utm_y: %f  avg utm_y: %f' %(avg_utm_y, avg_utm_y))
	rmse_utm_x = np.sqrt(((utm_x - avg_utm_x ) ** 2).mean())
	rmse_utm_y = np.sqrt(((utm_y - avg_utm_y) ** 2).mean())
	print('rmse utm_x: %f rmse utm_y: %f' %(rmse_utm_x,  rmse_utm_y))  


	fig1 = plt.figure(1)
	ax = fig1.add_subplot(111, projection='3d')
	Axes3D.scatter(ax, utm_x, utm_y, z_data, 'z', 20, None, True)
	ax.set_xlabel('utm_x')
	ax.set_ylabel('utm_y')
	ax.set_zlabel('time (s)')	
	plt.show()	


if __name__ == "__main__":
	readBagTopicList()	
