#!/usr/bin/env python

import sys
import lcm
import time
import serial
import utm
import exlcm


class imu_driver(object):
	def __init__(self, port_name):
	    self.port = serial.Serial(port_name, 115200, timeout=1.)
	    self.lcm = lcm.LCM()
	    self.packet = exlcm.imu_type()
            #cmd = "$VNWRG,07,40"
	    #self.port.write(cmd.encode('ascii')+'\r\n')
	    #out = self.port.read()
	    #print('response from device:' + out) 

		
	    
	
	def readloop(self):
	    while True:
		line = self.port.readline()
		vals = line.split(',')
		try:
		    self.packet.timestamp = time.time()*1e6
		    self.packet.magx = float(vals[2]) 
		    self.packet.magy = float(vals[3])
		    self.packet.magz = float(vals[4])
		    self.packet.accelx = float(vals[5])
		    self.packet.accely = float(vals[6])
		    self.packet.accelz = float(vals[7])
		    self.packet.gyrox = float(vals[8])
		    self.packet.gyroy = float(vals[9])
		    self.packet.gyroz = float(vals[10])		 		 
		    
		    print 'accel_x: %s, accel_y: %s, accel_z: %s'%(vals[5], vals[6], vals[7])
		    print self.packet.timestamp
		    self.lcm.publish("IMU", self.packet.encode())

		except Exception as e:
		    #exc_type, exc_value, exc_traceback = sys.exc_info()
		    print e
		   # print 'imu driver error (' + line + ')'		

if __name__ == "__main__":
	if len(sys.argv) != 2:
	   print "Usage: %s <serial_port>\n" % sys.argv[0]
	   sys.exit(0)
	my_imu = imu_driver(sys.argv[1])
	my_imu.readloop()


