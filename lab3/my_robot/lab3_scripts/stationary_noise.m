clear;clc;close all;
%% find noise characteristics of stationary data

load('accelx_stat.mat');
load('accely_stat.mat');
load('accelz_stat.mat');

load('gyrox_stat.mat');
load('gyroy_stat.mat');
load('gyroz_stat.mat');

load('magx_stat.mat');
load('magy_stat.mat');

%% accelerometers
bins=100;

subplot(1,3,1);
[n, xout] = hist(accelx,bins);
bar(xout, n, 'barwidth', 1, 'basevalue', 1);
set(gca,'YScale','log');
xlabel('m/s^2');
errx = std(accelx);
title('noise distribution of accel x');
legend(['error accel x: +/-' num2str(errx), ' m/s/s']);

subplot(1,3,2);
[n, xout] = hist(accely,bins);
bar(xout, n, 'barwidth', 1, 'basevalue', 1);
set(gca,'YScale','log');
xlabel('m/s^2');
erry = std(accely);
title('noise distribution of accel y');
legend(['error accel y: +/-' num2str(erry), ' m/s/s']);

subplot(1,3,3);
[n, xout] = hist(accelz,bins);
bar(xout, n, 'barwidth', 1, 'basevalue', 1);
set(gca,'YScale','log');
xlabel('m/s^2');
errz = std(accelz);
title('noise distribution of accel z');
legend(['error accel z: +/-' num2str(errz), ' m/s/s']);


%% gyros
xlab='rad/sec';

figure(2);
subplot(1,3,1);
[n, xout] = hist(gyrox,bins);
bar(xout, n, 'barwidth', 1, 'basevalue', 1);
set(gca,'YScale','log');
xlabel(xlab);
title('noise distribution of gyro x');
errgyrox = std(gyrox);
legend(['error gyrox: +/-' num2str(errgyrox), ' rad/s']);

subplot(1,3,2);
[n, xout] = hist(gyroy,bins);
bar(xout, n, 'barwidth', 1, 'basevalue', 1);
set(gca,'YScale','log');
xlabel(xlab);
title('noise distribution of gyro y');
errgyroy = std(gyroy);
legend(['error gyroy: +/-' num2str(errgyroy), ' rad/s']);

subplot(1,3,3);
[n, xout] = hist(gyroz,bins);
bar(xout, n, 'barwidth', 1, 'basevalue', 1);
set(gca,'YScale','log');
xlabel(xlab);
title('noise distribution of gyro z');
errgyroz = std(gyroz);
legend(['error gyroz: +/-' num2str(errgyroz), ' rad/s']);

%% magnetometers
xlab='Gauss';

figure(3);
subplot(1,2,1);
[n, xout] = hist(magx,bins);
bar(xout, n, 'barwidth', 1, 'basevalue', 1);
xlabel(xlab);
title('noise distribution of mag x');
errmagx = std(magx);
legend(['error magx: +/-' num2str(errmagx), ' Gauss']);

subplot(1,2,2);
[n, xout] = hist(magy,bins);
bar(xout, n, 'barwidth', 1, 'basevalue', 1);
xlabel(xlab);
title('noise distribution of mag y');
errmagy = std(magy);
legend(['error magy: +/-' num2str(errmagy), ' Gauss']);

figure(4);
subplot(1,2,1);
histfit(magx,bins,'kernel');
subplot(1,2,2);
histfit(magy);








