%% soft and hard iron corrections
clc;
clear;
close all;
load('magx.mat');
load('magy.mat');
load('yaw.mat');
circle_data_magx = magx(1,1700:5000);
circle_data_magy = magy(1,1700:5000);

%time = 1:numel(circle_data_magx); 
%used for plotting sensor data against time to see where specific events occurred in our trip

%% plotting raw data before any corrections
figure(1);
%subplot(2,2,1);
scatter(circle_data_magx, circle_data_magy)
hold on;
grid on;

%% perform hard iron corrections- shift circle to center
xmax = max(circle_data_magx);
xmin = min(circle_data_magx);

ymax = max(circle_data_magy);
ymin = min(circle_data_magy);


alpha = (xmax+xmin)/2;
beta = (ymax+ymin)/2;

hard_iron_corr_x = circle_data_magx-alpha;
hard_iron_corr_y = circle_data_magy-beta;

%subplot(2,2,2);
scatter(hard_iron_corr_x, hard_iron_corr_y);
grid on;
hold on;

%% perform soft iron corrections - find and apply scale and rotation corrections
magnitudexy = zeros(size(circle_data_magx,2),3); 
magnitudexy(:,2) = hard_iron_corr_x';
magnitudexy(:,3) = hard_iron_corr_y';
magnitudexy(:,1) = sqrt(magnitudexy(:,2).^2 + magnitudexy(:,3).^2);
max_mag = [];
min_mag = [];
[maxval, I] = max(magnitudexy(:,1));
[minval, J] = min(magnitudexy(:,1));

max_mag = [maxval, magnitudexy(I, 3)]; %corresponds to r, major axis
min_mag = [minval, magnitudexy(J, 3)]; %corresponds to q, minor axis

theta_major = asin(max_mag(1,2)/max_mag(1,1));
theta_minor = asin(min_mag(1,2)/min_mag(1,1));

R = [cos(theta_major), sin(theta_major); -sin(theta_major), cos(theta_major)]; %rotation matrix
v1 = R*(magnitudexy(:,2:3)'); %v1 dim should be 2x3301
scatter(v1(1,:), v1(2,:));
grid on;
hold on;

v1=v1'; %change v1 dim to 3301x2 (personal preference)
r = max(magnitudexy(:,2));
q = max(magnitudexy(:,3));

% r=max_mag(1,2);
% q=min_mag(1,2);
sigma = q/r;

mag_scaled_x = v1(:,1)./sigma;

v2 = [mag_scaled_x, v1(:,2)];%size v2 = 3301x2

%subplot(2,2,3);
scatter(v2(:,1), v2(:,2));
grid on;
hold on;

R1 = [cos(-theta_major), sin(-theta_major); -sin(-theta_major), cos(-theta_major)]; %rotate again to get back to original position
v3 = R1*v2';%apply rotation to data 2x2 x 2x3301
v3 = v3'; %3301x2
%subplot(2,2,4);
scatter(v3(:,1), v3(:,2));
grid on;
legend('raw', 'hard iron', 'rotated','soft iron corr', 'soft iron rotated back to orig pos');
title('applying hard & soft iron corrections to magnetometer x and y data');
xlabel('magnetometer x (Gauss)');
ylabel('magnetometer y (Gauss)');
hold off;
%% apply corrections to entire magnetometer dataset

% apply hard iron / shift correction 
shifted_x = magx-alpha;
shifted_y = magy-beta;

% apply rotation correction
s = [shifted_x;shifted_y]; %2x47000
rotate_full = R1*s;% 2x47000

% apply scaling correction
scaled_x = rotate_full(1,:)./sigma;
scaled_full = [scaled_x; rotate_full(2,:)];
figure(2);
scatter3(scaled_full(1,:), scaled_full(2,:), (1:numel(scaled_full(1,:)))./40);
xlabel('mag x (gauss)');
ylabel('mag y (gauss)');
zlabel('time (s)');
title('full magnetometer dataset corrected for hard & soft iron effects');

%% calculate yaw from magnetometer data
figure(3);
time = (1:numel(scaled_full(1,:)))./40;
yaw_full = atan2(-scaled_full(2,:),scaled_full(1,:));
plot(time, yaw_full);
xlabel('time (s)');
ylabel('heading (rad)');
title('Yaw Angle During Trip (in rad) Caculated from Corrected Magnetometer Data');
legend('heading using atan2');
hold on;
plot(time, yaw./(180/pi));
legend('magnetometer computed yaw', 'yaw from imu');














