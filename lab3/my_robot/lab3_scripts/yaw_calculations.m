clear;clc;close all;
%%integrate yaw rate sensor to find yaw angle
load('yaw_full.mat');
load('yaw.mat');
load('gyroz.mat');
x=gyroz;

% create the filter transfer function coefficients
fc = 0.01;
fs = 40;
[b,a]=butter(1, fc/(fs/2));
%freqz(b,a);

% apply the filter to the data
y = filter(b,a,x);
time = (1:numel(x))./40;
filtered_yaw_angle = cumtrapz(y);
filtered_yaw_angle = mod(filtered_yaw_angle,360);
% subplot(1,2,1);
% plot(time, y);
% title('yaw rate integrated to yaw angle using butterworth low pass filter');
% xlabel('time (s)');
% ylabel('yaw angle (deg)');
% 
% subplot(1,2,2);
% plot(time, x);
% title('yaw rate data with no filters applied');
% xlabel('time (s)');
% ylabel('yaw rate (deg/s)');

% trapezoidal numerical integration of yaw rate data
% subplot next to yaw angle computed via butterworth filter to compare
% the two methods
figure(2);
dx = 1/40;
trapint=[];
trapint(1)=0;
trapint(2)=0;
% for i=2:numel(x)-1
%     trapint(i) = (x(i-1) + x(i))*(dx/2) + trapint(i-1);    
% end
trapint=cumtrapz(gyroz);
trapint = mod(trapint,360);
subplot(1,2,1);
plot(time, trapint);
title('yaw angle computed via integration without low pass filter');
xlabel('time (s)');
ylabel('yaw angle (deg)');

subplot(1,2,2);
plot(time, filtered_yaw_angle);
title('yaw angle computed using butterworth low pass filter');
xlabel('time (s)');
ylabel('yaw angle (deg)');
%% complementary filter of imu calculated yaw rate and magnetometer-calculated yaw rate

yaw_complementary = [];
yaw_complementary(1) = 0;
yaw_complementary(2) = 0;

for i=2:(numel(x)-1)
    yaw_complementary(i) = 0.95*(yaw_complementary(i-1) + trapint(i)*dx) + 0.05*(yaw_full(i));
end
yaw_complementary = mod(yaw_complementary,360);
figure(3);
plot(time(1,1:end-1), yaw_complementary);
title('yaw angle via complementary filter');
xlabel('time (s)');
ylabel('yaw angle (deg)');

%***************** plot the yaw angle calculated from the magnetometer
%readings*********************
figure(4);
plot(time(1,1:end-1), yaw_complementary);
title('yaw angle');
xlabel('time(s)');
ylabel('yaw angle (deg)');
hold on;
plot(time, yaw);
legend('yaw from complementary filter', 'yaw from imu');





