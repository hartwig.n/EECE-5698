close; clc; close all;

%% question 1
load('accelx.mat');
load('accely.mat');
load('accelz.mat');
load('scaled_full.mat'); %corrected yaw rate measurements from magnetometer
load('yaw.mat');
load('gyroz.mat');
load('yaw_full.mat');
load('utmx.mat');
load('utmy.mat');

t = (1:numel(accelx))./40;


% plot(scaled_full(1,:), scaled_full(2,:));
% plot(1:numel(yaw), yaw);

X_dot = [];
X_dot(1) = 0;
X_dot(2) = 0;
% mean_accel_x = zeros(1,numel(accelx));
% mean_accel_x(:,:) = mean(accelx);

mean_accel_x = mean(accelx);
normalized_accelx = accelx - mean_accel_x;
shifted_accelx = normalized_accelx - (normalized_accelx(1,1));
plot(t, normalized_accelx);
hold on;
plot(t, shifted_accelx);
legend('mu sub', 'min shifted');
% plot(t, normalized_accelx);
% title('acceleration in x axis');
% xlabel('time (s)');
% ylabel('x acceleration (m/s^2)');
% legend('mean subtracted');

%integrate to find velocity x
for i = 2:numel(normalized_accelx)
    X_dot(i) = trapz(t(1:i),shifted_accelx(1:i));
end

figure(2);
plot(t, X_dot.*(-1));
title('velocity x vs time');
legend('vel');
xlabel('time (s)')
ylabel('velocity (m/s)');
X_dot = X_dot - (X_dot(1));


wX_dot = [];
wX_dot = gyroz.*X_dot;

figure(3);
plot(t,wX_dot);
title('calculated acceleration y');
xlabel('time (s)');
ylabel('omega*X_dot (m/s^2)');
hold on;
mean_accel_y = mean(accely);
normalized_accely = accely - mean_accel_y;
plot(t,normalized_accely);
xlabel('time (s)');
ylabel('accel y (m/s^2)');
legend('calculated acceleration y', 'observed acceleration y');
%% question 2
% displacement_x = cumtrapz(X_dot);
% figure(3);
% plot(t, displacement_x);
% title('displacement x');
% xlabel('time (s)');
% ylabel('displacement (m)');

yaw_radians = yaw_full*(pi/180);
vect_E = [];
vect_N = [];
pos_E = [];
pos_N = [];

figure(4);
%subplot(1,2,1);
vec_E = X_dot.*-cos(yaw_full); %yaw_full data is corrected with rotation matrix and shifting from magnetometer calculations
vec_N = X_dot.*-sin(yaw_full); %try using yaw angle from integration of gyroz
% pos_E = cumtrapz(vec_E);
% pos_N = cumtrapz(vec_N);
for i = 2:length(t)
    pos_E(i) = trapz(t(1:i),vec_E(1:i));
    pos_N(i) = trapz(t(1:i),vec_N(1:i));
end
plot((pos_E./70).*-1, pos_N./70); %apply scaling factor here if needed
%title('Car Position Using IMU');
xlabel('East Heading');
ylabel('North Heading');
hold on;
%subplot(1,2,2);
utm_x_shifted = utmx-(utmx(1));
utm_y_shifted = utmy-(utmy(1));
plot(utm_x_shifted, utm_y_shifted);
title('car position using GPS');
xlabel('utm x (m)');
ylabel('utm y (m)');
legend('trajectory via imu','trajectory via gps');


%question 3
figure(5);
x_center = (normalized_accely - wX_dot);    % observed acceleration - calculated acceleration
gyroz(end+1) = gyroz(end);                  
gyroz_diff = diff(gyroz);                   % take successive differences between values in gyroz vector
x_center = x_center./gyroz_diff;            % divide each value of x_center (error?) by differences between values in gyroz vector
x_center(~isfinite(x_center))=0; %replace values of infinity/-inf with 0
mu_x_center = mean(x_center./1000);
[n, xout] = hist(x_center./1000,100);
bar(xout, n, 'barwidth', 1, 'basevalue', 1);
set(gca,'YScale','log');
legend(['est. X_c: ' num2str(mu_x_center), 'meters']);

figure(6);
plot((pos_E./70).*-1, pos_N./70); %apply scaling factor here if needed
title('Car Position Using IMU');
xlabel('East Heading');
ylabel('North Heading');

    
