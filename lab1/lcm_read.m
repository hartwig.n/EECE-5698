% add the lcm.jar file to the matlabpath - need to only do this once
javaaddpath /home/nhartwig/EECE-5698/lab1/my_types.jar;
javaaddpath /home/nhartwig/lcm-1.3.1/lcm-java/lcm.jar;

%open my data log file
%18:53:59 = 10 mins of data in one spot
file_1 = lcm.logging.Log('/home/nhartwig/EECE-5698/lab1/data/lcm-log-2018-01-30-18:53:59', 'r'); 
file_2 = lcm.logging.Log('/home/nhartwig/EECE-5698/lab1/data/lcm-log-2018-01-30-19:12:53', 'r');

log_file = file_1;

% now read the file 
% here we are assuming that the channel we are interested in is RDI. Your channel 
% name will be different - something to do with GPS
% also RDI has fields altitude and ranges - GPS will probably have lat, lon, utmx,
% utmy etc

utm_x = [];
utm_y = [];
timestamp = [];
altitude = [];
i = 1;

while true
 try
   ev = log_file.readNext();
   
   % channel name is in ev.channel
   % there may be multiple channels but in this case you are only interested in GPS channel
   if strcmp(ev.channel, 'GPS')
     % build GPS object from data in this record
      GPS = exlcm.gps(ev.data);

      % now you can do things like depending upon the rdi_t struct that was defined
      utm_x(i) = GPS.utm_x;
      utm_y(i) = GPS.utm_y;
      timestamp(i) = GPS.timestamp;  % (timestamp in microseconds since the epoch)
      altitude(i) = GPS.altitude.value;      
      %fprintf('altitude: %f, timestamp: %f, utm_x: %f, utm_y: %f, \n', altitude, timestamp, utm_x, utm_y);
      i = i + 1;
    end
  catch err   % exception will be thrown when you hit end of file
     break;
 end
end
utm_x_offset_rem = utm_x - utm_x(1);
utm_y_offset_rem = utm_y - utm_y(1);

%gps_data_graph = scatter(utm_x_offset_rem, utm_y_offset_rem, 'r*');
%xlabel('utm x');
%ylabel('utm y');

actual_value_utm_x = mean(utm_x_offset_rem);
actual_value_utm_y = mean(utm_y_offset_rem);
scatter(actual_value_utm_x, actual_value_utm_y);

rms_error_utm_x = rms(utm_x_offset_rem - actual_value_utm_x);
rms_error_utm_y = rms(utm_y_offset_rem - actual_value_utm_y);

