#!/usr/bin/env python

import sys
import lcm
import time
import serial
import exlcm
import utm

class driver(object):
	def __init__(self, port_name):
	    self.port = serial.Serial(port_name, 4800, timeout=1.)
	    self.lcm = lcm.LCM()
	    self.packet = exlcm.gps()
	    
	
	def readloop(self):
	    while True:
		line = self.port.readline()
		vals = line.split(',')
		if vals[0] == "$GPGGA":
			try:
			    self.packet.timestamp = float(vals[1]) 
			    self.packet.latitude.value = float(vals[2])
			    self.packet.latitude.ns_indicator = vals[3]
			    self.packet.longitude.value = float(vals[4])
			    self.packet.longitude.ew_indicator = vals[5]
			    self.packet.altitude.value = float(vals[9])
 			    self.packet.altitude.units = vals[10]
 			    
			     
			    lat = vals[2]
			    lat_deg = float(lat[0:2])
			    lat_mins = float(lat[2:])
			    utm_lat = lat_deg + lat_mins/60
			    if self.packet.latitude.ns_indicator != 'N':
				utm_lat = utm_lat*-1

			    lon = vals[4]
			    lon_deg = float(lon[0:3])
			    lon_mins = float(lon[3:])
			    utm_lon = lon_deg + lon_mins/60
			    if self.packet.longitude.ew_indicator != 'E':
				utm_lon = utm_lon*-1

			    utm_coords = utm.from_latlon(utm_lat, utm_lon)
			    self.packet.utm_x = utm_coords[0]
			    self.packet.utm_y = utm_coords[1]
			    print 'lat: %f, long: %f'%(utm_lat, utm_lon)
			    print 'utm_lat: %f, utm_lon %f'%(utm_coords[0], utm_coords[1])
			    self.lcm.publish("GPS", self.packet.encode())

			except Exception as e:
			    #exc_type, exc_value, exc_traceback = sys.exc_info()
			    print e
			   # print 'gps driver error (' + line + ')'		

if __name__ == "__main__":
	if len(sys.argv) != 2:
	   print "Usage: %s <serial_port>\n" % sys.argv[0]
	   sys.exit(0)
	mygps = driver(sys.argv[1])
	mygps.readloop()


