%% image stitching using Matlab Example Code and Harris feature detector
clear;clc;close all;
%% step 1

% Load images.
%dir = fullfile('latino_center_building_undistorted');
dir = fullfile('latino_center_undistorted_color');
%dir = fullfile('other_mural_undistorted');
%dir = fullfile('brick_wall_undistorted');
buildingDir = dir;
buildingScene = imageDatastore(buildingDir);

% Display images to be stitched
montage(buildingScene.Files)

%% step 2

% Read the first image from the image set.
I = readimage(buildingScene, 1);

% attempt 1: harris function call options: [x,y,m] = harris(I,1000,'tile',[30 30], 'disp', 'thresh',1000);
% attempt 2: [points(:,1),points(:,2),m] = harris(grayImage,100000,'tile',[30 30], 'disp', 'thresh',1000);
% attempt 3: 
% Initialize features for I(1)
grayImage = rgb2gray(I);
points = [];
[points(:,1),points(:,2),m] = harris(grayImage, 10000,'tile',[20 20],'disp');

%points = detectSURFFeatures(grayImage);
[features, points] = extractFeatures(grayImage, points);

% Initialize all the transforms to the identity matrix. Note that the
% projective transform is used here because the building images are fairly
% close to the camera. Had the scene been captured from a further distance,
% an affine transform would suffice.
numImages = numel(buildingScene.Files);
tforms(numImages) = projective2d(eye(3));

% Iterate over remaining image pairs
for n = 2:numImages

    % Store points and features for I(n-1).
    pointsPrevious = points;
    points=[]; % reset size of points to prevent dimension mismatch with harris function call
    featuresPrevious = features;

    % Read I(n).
    I = readimage(buildingScene, n);
    
    grayImage = rgb2gray(I);
    
    [points(:,1),points(:,2),m] = harris(grayImage, 10000,'tile',[20 20],'disp');
   
    [features, points] = extractFeatures(grayImage, points);

    % Find correspondences between I(n) and I(n-1).
    indexPairs = matchFeatures(features, featuresPrevious, 'Unique', true);

    matchedPoints = points(indexPairs(:,1), :);
    matchedPointsPrev = pointsPrevious(indexPairs(:,2), :);

    % Estimate the transformation between I(n) and I(n-1).
    tforms(n) = estimateGeometricTransform(matchedPoints, matchedPointsPrev,...
        'projective', 'Confidence', 99.9, 'MaxNumTrials', 200000);

    % Compute T(n) * T(n-1) * ... * T(1)
    tforms(n).T = tforms(n).T * tforms(n-1).T;
end
 

% step 3 
imageSize = size(I);  % all the images are the same size

% Compute the output limits  for each transform
for i = 1:numel(tforms)
    [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(2)], [1 imageSize(1)]);
end

avgXLim = mean(xlim, 2);

[~, idx] = sort(avgXLim);

centerIdx = floor((numel(tforms)+1)/2);

centerImageIdx = idx(centerIdx);

Tinv = invert(tforms(centerImageIdx));

for i = 1:numel(tforms)
    tforms(i).T = tforms(i).T * Tinv.T;
end


%% step 4
imageSize = size(I);
for i = 1:numel(tforms)
    [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(2)], [1 imageSize(1)]);
end

% Find the minimum and maximum output limits
xMin = min([1; xlim(:)]);
xMax = max([imageSize(2); xlim(:)]);

yMin = min([1; ylim(:)]);
yMax = max([imageSize(1); ylim(:)]);

% Width and height of panorama.
width  = round(xMax - xMin);
height = round(yMax - yMin);


% changed zeros matrix to height by width as images are grayscale
% Initialize the "empty" panorama.
panorama = zeros([height width 3], 'like', I);

%% step 5
blender = vision.AlphaBlender('Operation', 'Binary mask', ...
    'MaskSource', 'Input port');

% Create a 2-D spatial reference object defining the size of the panorama.
xLimits = [xMin xMax];
yLimits = [yMin yMax];
panoramaView = imref2d([height width], xLimits, yLimits);

% Create the panorama
for i = 1:numImages

    I = readimage(buildingScene, i);
    
    grayImage = rgb2gray(I);

    % Transform I into the panorama.
    warpedImage = imwarp(I, tforms(i), 'OutputView', panoramaView);
    
    %Find centroid of the image used as the center image (centerIdx)
    if i==centerIdx
        whiteSquare = imbinarize(warpedImage(:,:,1),0);
        whiteSquare = imfill(whiteSquare,'holes');
        c = regionprops(whiteSquare, 'centroid');
        centroid = cat(1,c.Centroid);        
        centerx = centroid(1,1);
        centery = centroid(1,2);
    end   

    % Generate a binary mask.
    mask = imwarp(true(size(grayImage,1),size(grayImage,2)), tforms(i), 'OutputView', panoramaView);
         
    % Overlay the warpedImage onto the panorama.
    panorama = step(blender, panorama, warpedImage, mask);
    imshow(panorama);
end

figure
imshow(panorama)
hold on;

%plot camera position
for i=1:numImages    
    plot(tforms(i).T(3,1)+centerx,tforms(i).T(3,2)+centery,'gO','MarkerSize',5);
    hold on;
end
figure;
imshow(panorama);
