% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 551.435630744020955 ; 554.320905545230517 ];

%-- Principal point:
cc = [ 316.613461767677336 ; 235.939553413449573 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.071162728464610 ; -0.048289522155233 ; -0.000287596998476 ; -0.000538197203252 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 1.500402840019814 ; 1.544705666444141 ];

%-- Principal point uncertainty:
cc_error = [ 1.384408996683087 ; 1.073184582879064 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.007334693335954 ; 0.029034647409037 ; 0.000725216987445 ; 0.000948978074882 ; 0.000000000000000 ];

%-- Image size:
nx = 640;
ny = 480;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 14;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -2.218140e+00 ; -2.213461e+00 ; 8.329994e-03 ];
Tc_1  = [ -9.746917e+01 ; -6.825781e+01 ; 2.832219e+02 ];
omc_error_1 = [ 1.655730e-03 ; 1.739283e-03 ; 3.596674e-03 ];
Tc_error_1  = [ 7.213506e-01 ; 5.594237e-01 ; 8.478039e-01 ];

%-- Image #2:
omc_2 = [ -2.219363e+00 ; -2.199668e+00 ; 9.001932e-03 ];
Tc_2  = [ -9.707133e+01 ; -6.667552e+01 ; 2.820399e+02 ];
omc_error_2 = [ 1.644270e-03 ; 1.735030e-03 ; 3.575171e-03 ];
Tc_error_2  = [ 7.180090e-01 ; 5.570787e-01 ; 8.437519e-01 ];

%-- Image #3:
omc_3 = [ -2.036003e+00 ; -2.052221e+00 ; 2.933762e-01 ];
Tc_3  = [ -9.524212e+01 ; -6.274735e+01 ; 3.496946e+02 ];
omc_error_3 = [ 1.818699e-03 ; 1.902990e-03 ; 3.533258e-03 ];
Tc_error_3  = [ 8.710738e-01 ; 6.768858e-01 ; 1.015743e+00 ];

%-- Image #4:
omc_4 = [ 2.259739e+00 ; 1.843855e+00 ; -6.261109e-01 ];
Tc_4  = [ -1.164163e+02 ; -5.601204e+01 ; 3.877631e+02 ];
omc_error_4 = [ 1.638071e-03 ; 1.930859e-03 ; 3.898921e-03 ];
Tc_error_4  = [ 9.711555e-01 ; 7.547948e-01 ; 1.092614e+00 ];

%-- Image #5:
omc_5 = [ -1.884979e+00 ; -1.916255e+00 ; 4.123067e-01 ];
Tc_5  = [ 7.365518e+00 ; -1.011877e+02 ; 4.266278e+02 ];
omc_error_5 = [ 2.064184e-03 ; 2.113591e-03 ; 3.672428e-03 ];
Tc_error_5  = [ 1.078916e+00 ; 8.192056e-01 ; 1.205908e+00 ];

%-- Image #6:
omc_6 = [ -1.653704e+00 ; -2.134555e+00 ; 6.155819e-01 ];
Tc_6  = [ -2.666768e+01 ; -7.740081e+01 ; 4.800899e+02 ];
omc_error_6 = [ 1.936508e-03 ; 2.285376e-03 ; 3.658402e-03 ];
Tc_error_6  = [ 1.204430e+00 ; 9.244842e-01 ; 1.269244e+00 ];

%-- Image #7:
omc_7 = [ -2.074509e+00 ; -2.135412e+00 ; 6.300604e-01 ];
Tc_7  = [ -6.017747e+01 ; -6.816750e+01 ; 4.472114e+02 ];
omc_error_7 = [ 2.254915e-03 ; 1.926902e-03 ; 4.380396e-03 ];
Tc_error_7  = [ 1.121741e+00 ; 8.596814e-01 ; 1.219954e+00 ];

%-- Image #8:
omc_8 = [ 2.016313e+00 ; 2.003173e+00 ; -9.778701e-01 ];
Tc_8  = [ -8.262992e+01 ; -4.580463e+01 ; 4.572602e+02 ];
omc_error_8 = [ 1.536677e-03 ; 2.162066e-03 ; 3.824451e-03 ];
Tc_error_8  = [ 1.144544e+00 ; 8.879425e-01 ; 1.156052e+00 ];

%-- Image #9:
omc_9 = [ 1.993971e+00 ; 1.771385e+00 ; -7.963731e-01 ];
Tc_9  = [ -9.902063e+01 ; -5.982875e+01 ; 4.208877e+02 ];
omc_error_9 = [ 1.602919e-03 ; 2.062570e-03 ; 3.518387e-03 ];
Tc_error_9  = [ 1.054581e+00 ; 8.201327e-01 ; 1.120430e+00 ];

%-- Image #10:
omc_10 = [ -1.561269e+00 ; -2.398668e+00 ; -6.695174e-01 ];
Tc_10  = [ -4.232829e+01 ; -6.929419e+01 ; 2.960744e+02 ];
omc_error_10 = [ 1.070989e-03 ; 2.486017e-03 ; 3.378728e-03 ];
Tc_error_10  = [ 7.518165e-01 ; 5.877874e-01 ; 9.968585e-01 ];

%-- Image #11:
omc_11 = [ 2.253677e+00 ; 1.830359e+00 ; -6.283098e-01 ];
Tc_11  = [ -1.136550e+02 ; -2.828942e+01 ; 4.236618e+02 ];
omc_error_11 = [ 1.837907e-03 ; 1.969068e-03 ; 4.094629e-03 ];
Tc_error_11  = [ 1.057067e+00 ; 8.242699e-01 ; 1.181834e+00 ];

%-- Image #12:
omc_12 = [ -2.244181e+00 ; -2.051042e+00 ; 3.353721e-01 ];
Tc_12  = [ -8.245330e+01 ; -3.425958e+01 ; 4.442350e+02 ];
omc_error_12 = [ 2.453357e-03 ; 2.491801e-03 ; 5.425015e-03 ];
Tc_error_12  = [ 1.109003e+00 ; 8.571524e-01 ; 1.297374e+00 ];

%-- Image #13:
omc_13 = [ -2.204364e+00 ; -2.177187e+00 ; 1.302368e-01 ];
Tc_13  = [ -1.115197e+02 ; -7.177547e+01 ; 2.868891e+02 ];
omc_error_13 = [ 1.741967e-03 ; 1.632318e-03 ; 3.502999e-03 ];
Tc_error_13  = [ 7.236789e-01 ; 5.650764e-01 ; 8.748955e-01 ];

%-- Image #14:
omc_14 = [ 2.280609e+00 ; 2.063482e+00 ; -5.797485e-01 ];
Tc_14  = [ -1.092369e+02 ; -4.121734e+01 ; 4.177905e+02 ];
omc_error_14 = [ 1.865712e-03 ; 2.053006e-03 ; 4.392486e-03 ];
Tc_error_14  = [ 1.040994e+00 ; 8.109334e-01 ; 1.178834e+00 ];

